
-P dev clean package   -Dmaven.test.skip=true
文档地址  http://git.oschina.net/catshen/zscatDocument
#pig
dubbo 端口
cms  20880
mall  20881
oa  20882
web  20883
mall-portl  20884

tomcat 端口
web  8080
mall-portl  8083
rest 8081
sso 8082

打包命令 
开发环境  mvn -P dev clean package   -Dmaven.test.skip=true
测试环境  mvn -P testclean package   -Dmaven.test.skip=true
生产环境  mvn -P uat clean package   -Dmaven.test.skip=true


http://my.oschina.net/xianggao/blog/637807
（1）业务拆分后：每个子系统需要单独的库；

（2）如果单独的库太大，可以根据业务特性，进行再次分库，比如商品分类库，产品库；

（3）分库后，如果表中有数据量很大的，则进行分表，一般可以按照Id，时间等进行分表；（高级的用法是一致性Hash）

（4）在分库，分表的基础上，进行读写分离；


（1）用户下单后，写入消息队列，后直接返回客户端；

（2）库存子系统：读取消息队列信息，完成减库存；

（3）配送子系统：读取消息队列信息，进行配送；
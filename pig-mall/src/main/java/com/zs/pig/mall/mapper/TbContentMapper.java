package com.zs.pig.mall.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.mall.api.model.TbContent;

public interface TbContentMapper  extends Mapper<TbContent> {
   
}
package com.zs.pig.mall.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.mall.api.model.TbOrderShipping;

public interface TbContentCategoryMapper  extends Mapper<TbOrderShipping> {
    
}
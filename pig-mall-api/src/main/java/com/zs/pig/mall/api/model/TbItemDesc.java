package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;
/**
 * 2016-04-22
 * @author zsCat
 */

import com.zs.pig.common.base.BaseEntity;

@SuppressWarnings({ "unused"})
@Table(name="tb_item_desc")
public class TbItemDesc extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private Long itemId;
    private Date created;

    private Date updated;

    private String itemDesc;

    public Long getItemId() {
        return this.getLong("itemId");
    }

    public void setItemId(Long itemId) {
        this.set("itemId", itemId);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }

    public String getItemDesc() {
        return this.getString("itemDesc");
    }

    public void setItemDesc(String itemDesc) {
        this.set("itemDesc", itemDesc);
    }
}
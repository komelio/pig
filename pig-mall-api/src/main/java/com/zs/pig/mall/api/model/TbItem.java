package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_item")
public class TbItem extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private String title;

    private String sellPoint;

    private Long price;

    private Integer num;

    private String barcode;

    private String image;

    private Long cid;

    private Byte status;

    private Date created;

    private Date updated;

    

    public String getTitle() {
        return this.getString("title");
    }

    public void setTitle(String title) {
        this.set("title", title);
    }

    public String getSellPoint() {
        return this.getString("sellPoint");
    }

    public void setSellPoint(String sellPoint) {
        this.set("sellPoint", sellPoint);
    }

    public Long getPrice() {
        return this.getLong("price");
    }

    public void setPrice(Long price) {
        this.set("price", price);
    }

    public Integer getNum() {
        return this.getInteger("num");
    }

    public void setNum(Integer num) {
        this.set("num", num);
    }

    public String getBarcode() {
        return this.getString("barcode");
    }

    public void setBarcode(String barcode) {
        this.set("barcode", barcode);
    }

    public String getImage() {
        return this.getString("image");
    }

    public void setImage(String image) {
        this.set("image", image);
    }

    public Long getCid() {
        return this.getLong("cid");
    }

    public void setCid(Long cid) {
        this.set("cid", cid);
    }

    public Byte getStatus() {
        return this.getByte("status");
    }

    public void setStatus(Byte status) {
        this.set("status", status);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }
}
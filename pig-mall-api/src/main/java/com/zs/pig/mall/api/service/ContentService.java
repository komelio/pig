package com.zs.pig.mall.api.service;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.mall.api.model.TbContent;

public interface ContentService extends BaseService<TbContent>{

	PigResult insertContent(TbContent content);
}

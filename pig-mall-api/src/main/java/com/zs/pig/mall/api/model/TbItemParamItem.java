package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_item_param_item")
public class TbItemParamItem extends BaseEntity {

	private static final long serialVersionUID = 1L;
    private Long itemId;

    private Date created;

    private Date updated;

    private String paramData;

   

    public Long getItemId() {
        return this.getLong("itemId");
    }

    public void setItemId(Long itemId) {
        this.set("itemId", itemId);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }

    public String getParamData() {
        return this.getString("paramData");
    }

    public void setParamData(String paramData) {
        this.set("paramData", paramData);
    }
}